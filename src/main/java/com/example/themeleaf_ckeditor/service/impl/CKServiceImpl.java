package com.example.themeleaf_ckeditor.service.impl;

import com.example.themeleaf_ckeditor.model.CkText;
import com.example.themeleaf_ckeditor.repository.CKRepository;
import com.example.themeleaf_ckeditor.service.CKService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CKServiceImpl implements CKService {

    @Autowired
    private CKRepository ckRepository;

    @Override
    public CkText findAdminById(int id) {
        return ckRepository.findAdminById(id);
    }

    @Override
    public List<CkText> getAllAdmin() {
        return ckRepository.findAll();
    }

    @Override
    public boolean insertAdmin(CkText ckText) {
        return ckRepository.insertAdmin(ckText);
    }
}
