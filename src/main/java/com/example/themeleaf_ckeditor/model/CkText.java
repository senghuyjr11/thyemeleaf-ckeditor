package com.example.themeleaf_ckeditor.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Setter
@Getter
@ToString
@Accessors(chain = true)
public class CkText {
    private int id;
    private String description;
    private String emoji;
    private String image;
}
