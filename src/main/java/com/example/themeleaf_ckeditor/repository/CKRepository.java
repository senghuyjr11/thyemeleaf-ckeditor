package com.example.themeleaf_ckeditor.repository;

import com.example.themeleaf_ckeditor.model.CkText;
import com.example.themeleaf_ckeditor.repository.provider.CKProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CKRepository {
    @Select("select * from admin")
    @ResultMap("adminMapping")
    List<CkText> findAll();

    @Insert("insert into admin (id, description) values (#{id}, #{description})")
    boolean insertAdmin(CkText ckText);

    @SelectProvider(type = CKProvider.class, method = "getAdmin")
    @Results(id = "adminMapping", value = {
            @Result(property = "description", column = "description")
    })
    CkText findAdminById(@Param("id") Integer id);
}
