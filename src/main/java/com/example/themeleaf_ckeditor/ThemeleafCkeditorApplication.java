package com.example.themeleaf_ckeditor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThemeleafCkeditorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThemeleafCkeditorApplication.class, args);
    }

}
